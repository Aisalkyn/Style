from django.views.generic import TemplateView

class HomeView(TemplateView):
    template_name = "home.html"

class ContactsView(TemplateView):
    template_name = "contacts.html"

class ProductsView(TemplateView):
    template_name = "products.html"

class MyPageView(TemplateView):
    template_name = "my_page.html"

class ProfilePage(TemplateView):
    template_name = "registration/profile.html"
